# Introduction

This example shows how you can load discrete audio files without a real video source. We use `EmptySource` from the `@accurate-player/mse` package to create an empty video 
and use that as our master source in the player.

```typescript
import { EmptySource } from "@accurate-player/mse";

// Create an empty video source with 600 second duration
const emptySource = new EmptySource(600);

console.log(emptySource.videoFile);
// Output:
// {
//     id: "...",
//     src: "..."
//     label: "Empty source",
//     duration: 22200.8,
//     frameRate: {
//         numerator: 25,
//         denominator: 1
//     },
//     dropFrame: false,
//     channelCount: 0,
// }

// emptySource.videoFile is a VideoFile object ready to be loaded by the player
player.api.loadVideoFile(emptySource.videoFile);
```

Now that we have a master source, we can use all plugins as we normally would, for example load a couple of discrete audio files:

```typescript
discreteAudioPlugin.discreteAudioPlugin.initDiscreteAudioTracks(...);
```

In the example in this repository we set the duration of the `EmptySource` to the longest of our discrete audio files to make sure we can play all discrete audio files to 
full length. The main source code for the example can be found in `src/Application.ts` check that out for details. The example looks like this:

!["the example opened in the browser"](docs/demo.png)

# Run the example

1. Clone the repository

   ```
   git clone git@gitlab.com:accurate-player/ap-audio-only-example.git
   ```

1. Install dependencies

    ```
    yarn
    ```

1. Setup your license key

    ```
    cp shared/license-key-example.js shared/license-key.js
    ```
   
    Edit `shared/license-key.js` and insert your license key:

    ```js
    window.LICENSE_KEY = "YOUR_LICENSE_KEY";
    ```

1. Start the development server

    ```
    yarn start
    ```

1. Open the demo in your browser:

   ```
   open http://localhost:3000
   ```
