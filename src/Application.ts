import { css, customElement, html, LitElement, property, PropertyValues, query, TemplateResult } from 'lit-element';
import { ProgressivePlayer as AccuratePlayer } from "@accurate-player/accurate-player-progressive"
import { DiscreteAudioPlugin, HotkeyPlugin } from "@accurate-player/accurate-player-plugins";
import { EmptySource } from "@accurate-player/mse";

import "@accurate-player/accurate-player-controls";

/**
 * Application root element.
 */
@customElement('av-application')
export class Application extends LitElement {
	static styles = css`
    :host {
      background: var(--AP-BACKGROUND-BODY);
      display: flex;
      padding: 20px;
      flex-direction: column;
      width: calc(100% - 40px);
      height: calc(100% - 40px);
    }

    div.container-video {
      display: flex;
      justify-content: center;
      background: #000;
      min-height: 0;
      min-width: 0;
      position: relative;
    }

    #id-master {
	  min-height: 30vh;
      min-width: 70vw;
      max-width: 100%;
      max-height: 60vh;
    }
  `;

	@query("#id-master")
	master: HTMLVideoElement | undefined;

	@query("apc-controls")
	controls: any;

	@property()
	private player: AccuratePlayer | undefined;

	private hotkeyPlugin: HotkeyPlugin;
	private discreteAudioPlugin: DiscreteAudioPlugin;

	protected firstUpdated(changedProperties: PropertyValues) {
		super.firstUpdated(changedProperties);
		this.initializeAccuratePlayer();
	}

	protected render(): TemplateResult {
		return html`
			<div class="container-video">
				<video id="id-master" crossorigin="anonymous"></video>
				<apc-controls></apc-controls>
			</div>
			<div>
				<p>In this example we've loaded two discrete audio files without a video source. You can use the player controls in the top right corner, the speech bubble, to toggle the discrete audio files on and off.</p>
				<p>We use <code>EmptySource</code> from the <code>@accurate-player/mse</code> package to load an empty video. After we've done that we can use the <code>DiscreteAudio</code> plugin to load discrete audio files as normal. For details, see <code>src/Application.ts</code>.</p>
			</div>
		`;
	}


	private initializeAccuratePlayer() {
		if (this.master == null) {
			console.warn("Failed to initialize Accurate Player: video element could not be found.");
			return;
		}
		if (window.LICENSE_KEY == null) {
			console.warn("Failed to initialize Accurate Player: no license key found, did you provide one in license-key.js?");
			return;
		}
		if (this.controls == null) {
			console.warn("Failed to initialize Accurate Player: could not find <apc-controls />");
			return;
		}

		// Initialize player and plugins
		this.player = new AccuratePlayer(this.master, window.LICENSE_KEY);
		this.hotkeyPlugin = new HotkeyPlugin(this.player);
		this.discreteAudioPlugin = new DiscreteAudioPlugin(this.player);

		// Load audio files
		this.loadAudioFiles();

		// The Polymer 3.0 project is currently written in Javascript. Will be converted to TypeScript with typings shortly!
		this.controls.init(this.master, this.player, {
			saveLocalSettings: true // Enables storing of simple settings as volume, timeFormat and autoHide.
		});
	}

	private loadAudioFiles(): void {
		// Our audio files we want to load
		const audioFiles = [
			{
				duration: 888.032,
				src: "https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/aac-discrete/sintel_stereo.mp4",
				label: "Sintel (Stereo)",
				channels: 2,
			},
			{
				duration: 734,
				src: "https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/aac-discrete/TOS-Stereo.mp4",
				label: "Tears of Steel (Stereo)",
				channels: 2,
			}
		];

		// Find the audio file with the longest duration
		const longestAudioFile = audioFiles.reduce((p, c) => p.duration < c.duration ? c : p);

		// Initialize an empty video source with the duration set to the maximum duration among our audio files
		// the EmptySource objects generates a black frame video for the given duration in the browser
		const emptySource = new EmptySource(longestAudioFile.duration);

		// The videoFile property is a VideoFile object that can be loaded into the player as is
		console.log(emptySource.videoFile);

		// Load the empty video source in the player, but change the label
		this.player?.api.loadVideoFile({
			...emptySource.videoFile,
			label: "Audio only example"
		});

		// Load the discrete audio tracks
		this.discreteAudioPlugin.initDiscreteAudioTracks(audioFiles.map(({src, channels, label}) => ({
			enabled: longestAudioFile.src === src,
			track: {
				id: src,
				src,
				channels,
				label
			}
		})));
	}
}

declare global {
	interface Window {
		LICENSE_KEY: string;
	}
}
